---
title: infra_rez
date: Wed Nov 2 05:00:00 CDT 2022
tldr: Infrastructure as code is the way.
tags:
- infrastructure
author: Brandon
images: /img/infra.jpg
draft: false
---
# infra_rez

My feeble second attempt at Infrastructure as Code. This is mostly personal projects with alot of projects working to be production capable.

I use tools such as Ansible, Terraform, Hugo and Docker. I have small collection of scripts. A lttle bit of everything!

Check out the [infra_rez](https://gitlab.com/infra_rez) on my gitlab. Always open to suggestions!

##### Below is a running list of my infrastructure.

## Laptop
- Thinkpad T440p
- Pinebook Pro

## Linode
| name | plan | OS |
|-|-|-|
| brandonia.xyz | 1GB Nanode | Alma 9 |
| proxy | 1GB Nanode | Alma 9 |
| util | 1GB Nanode | Alma 9 |
