---
title: anime-tracker
date: Wed Apr 28 22:58:09 CDT 2021
tldr: Track your anime progress into a spreadsheet from the command line.
tags:
- python
- anime
author: Brandon
images: /img/anime-tracker.jpg
---

A silly script I made for the command line using python, to track my progress of anime series into a spreadsheet. It was spawned by the desire to scratch my own itch and was a good learning opportunity as a result! I cannot attest to how well this is written but it works for me as intended. I usually use it in batches inside a script every once in a while. I also find it convenient to make a .bashrc alias for the file argument.

I pushed the script up to my get lab, go ahead and take a look; [anime-tracker](https://gitlab.com/brandonia/anime-tracker). Feel free to give me any feedback!