---
title: Arch Installation 2024
description: 
date: Sun Jan 21 12:52:56 CST 2024
tldr: How I install Arch Linux
draft: false
tags: 
  - Arch Linux
  - Linux
images: /img/arch2024.png
author: brandon
---

# Arch Install
## Objective
This file contains my notes for installing Arch Linux.

## Notes
- Linx browser exists in Arch iso

## Installation

### Make bootable ISO
- I download the Arch iso and put it on a external hard drive with Ventoy setup

#### WiFi
- Look for you network adaper

	```sh
	# displpay network adapter info
	ip link
	```

- Use `iwctl` to connect to your wireless access point

	```sh
	# config network with iwctl
	iwctl
	station $DEVICE scan
	station $DEVICE get-networks
	station $DEVICE connect $SSID
	```

### Create Partitons
- Partitions needed

|          | Partition | Mount Point | Size  |
|----------|-----------|-------------|-------|
| required | boot      | /mnt/boot   | 512MB |
| required | root      | /mnt        | 8GB+  |
| optional | home      | /mnt/home   | 1G+   |

- I used `fdisk` to complete this part.

	```sh
	# open disk up in fdisk
	fdisk /dev/sdX
	fdisk /dev/nvmeX
	fdisk $DEVICE
	```

- `fdisk` options reference

	```sh
	# options
	o - blank out partition table
	g - setup gpt scheme
	n - new partition
	t - change type
	w - write to partition table
	```

- If you want to use LVM, you will need to create the partitions

### Encryption
- After you are done partitioning your disks, you can encrypt your devices.

- Encrypt before pv/vg/lv creation and filesystem creation

- Use `cryptsetup` to encrypt devices

	```sh
	# encrypt partition
   	cryptsetup luksFormat -v -s 512 -h SHA512 $DEVICE
   	cryptsetup luksFormat /dev/sdX1
	
	# unlock encrypted partition
	cryptsetup open --type luks /dev/sdX1 lv_home
	```

### LVM
- Setup LVM on partitions after encryption
- TODO: LVM.md (needs updating)

### Create Filesystems
 - Root Filesystem (/)

	```sh
	# create ext4 filesystem
	mkfs.ext4 $DEVICE
	mkfs.ext4 /dev/sdX

	# create btrfs filesystem
	mkfs.btrfs -L archlinux $DEVICE
	```

 - boot partition (/boot)

	```sh
	# create vfat partition
	mkfs.vfat -F32 $DEVICE
	mkfs.vfat -F32 /dev/sdX1
	```

- **Optional:** home partition (/home)

### btrfs

- Create btrfs 
- Create btrfs root subvolume volume
- Create btrfs home subvolume

```sh
# create btrfs filesystem
mkfs.btrfs -L archlinux $DEVICE

# mount btrfs filesystem

# create subvolumes
cd /mnt
btrfs subvolume create @
btrfs subvolume create @home
```

##### Note
- seems that you need to mount btrfs fillsystem at the device level first. anytime you decide to go back and create another subvolume (ie. swap) you will need to 
- unmount the device before mounting the subolvumes 

### Mount filesystems
- Mount root filesystem to /mnt
- Mount the EFI System Partition (esp)
	- mkdir /mnt/boot
	- mount esp to /mnt/boot
- Mount home partition

```sh
sudo mount -o noatime,compress=zstd:1,space_cache=v2,discard=async,subvol=@swap /dev/mapper/luks /
sudo mount -o noatime,compress=zstd:1,space_cache=v2,discard=async,subvol=@swap /dev/mapper/luks /home
```

### Generate fstab
- Sanity check
	
```sh
# generate fstab
genfstab -U -p /mnt >> /mnt/etc/fstab
```

### Setup Mirrors
- Edit /etc/pacman.d/mirrorlist
- Top 6 mirrors will be used to intall system

### Pacstrap
- Its a good idea to install some important software at this time. 

```sh
# pacstrap and install packages
pacstrap /mnt base base-devel linux linux-headers linux-firmware vim \
		networkmanager grub efibootmgr mtools osprober dosfstools git \
		make python sudo fakeroot man-db man-pages dialog netctl \
		lvm2
```

### Chroot
```sh
arch-chroot /mnt
```

### mkinitcpio
- edit /etc/mkinitcpio

```sh
HOOKS=(. . . block encrypt lvm2 filesystems btrfs . . .) run 
```

- run mkinitcpio

```sh
mkinitcpio -p linux
```

### Set locale

```sh
# edit config file
vim /etc/locale.gen

# generate local
locale-gen
```

### Set timezone
```sh
# link your localtime
ln -s /usr/share/zoneinfo/*/* /etc/localtime

# sync system to hardware clock
hwclock --systohc --utc
```

### Networking
- Enable NetworkManager.service
- Start service
- nmtui to set up wifi or manage connections

###  Update

## Create users and groups and set passwords
- Set root password
- Add your User
```
useradd -m -g users -G wheel \<user>
```
- change password
- visudo and edit the sudoer file to let users in the group wheel be able to execute with root priveleges
- Add your user to group wheel
	
### Bootloader
#### systemd loader

#### grub
- Encryption
   - /etc/default/grub

   > GRUB_CMDLINE_LINUX_DEFAULT="loglevl=3 cryptdevice=\<dev>:\<vg>:allow-discards queit"

- enable corectrl to get full control of amdgpu

   > GRUB_CMDLINE_LINUX_DEFAULT="<other_params>... amdgpu.ppfeaturemask=0xffffffff"


- Install groub to esp
```
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
```

- mkdir /boot/grub/locale

- Generate configuration
```
grub-mkconfig -o /boot/grub/grub.cfg
```

## Optional: Create Swap File

### btrfs swap

```sh
# create swap subvolume
mount $MAPPER /mnt
btrfs subvolume create /mnt/@swap
umount /mnt

# create swapfile
btrfs filesystem mkswapfile --size 4g --uuid clear /swap/swapfile

# turn on swap
swapon /swap/swapfile

# fstab
/swap/swapfile none swap defaults 0 0

```

## Graphical Enviroment
- Install xorg
- Install DE or Display Manager + Window Manager
- Configure	
	
