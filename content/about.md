---
title: "About"
author: Brandon
date: Wed Jul  8 10:16:59 CDT 2020
images: /img/aboutmeHero.jpg
type: static
---

{{< figure src="/img/aboutme.jpg" class="aboutmeIMG">}}

Hello! 

My name is **Brandon Steffe**.

I'm a self taught technology enthusiast. I enjoy working with technology in any form. My focus is to continue to learn and enhance my skills within IT, particularly server administration or DevOps.  

##### Contact

---

If you want to check out some of my code, you can catch me over on **GitLab**. There I have my various works and my own build of dwm. Feel free to send me an email if you'd like or reach out to me on **Telegram**. I am always open to suggestions! 

